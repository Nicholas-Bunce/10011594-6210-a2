<?php

    class Addpages extends Controller {

        public function __construct() {
            $this->addpagemodel = $this->model('addpage');
        }


        public function about() {
            $Sub = $this->addpagemodel->Sub();
            $navinfo = $this->addpagemodel->navLinks();
            $about = $this->addpagemodel->about();

            $data = [
                'navSub' => $Sub,
                'navinfo1' => $navinfo,
                'about' => $about
            ];

            $this->view('addpages/about', $data);
        }

        public function FAQ() {
            $Sub = $this->addpagemodel->Sub();
            $navinfo = $this->addpagemodel->navLinks();
            $faq = $this->addpagemodel->FAQ();

            $data = [
                'faq' => $faq,
                'navSub' => $Sub,
                'navinfo1' => $navinfo
            ];

            $this->view('addpages/faq', $data);
        }

        public function External() {
            $Sub = $this->addpagemodel->Sub();
            $navinfo = $this->addpagemodel->navLinks();
            $exter = $this->addpagemodel->External();

            $data = [
                'external' => $exter,
                'navSub' => $Sub,
                'navinfo1' => $navinfo
            ];

            $this->view('addpages/External', $data);
        }

        public function Policy() {
            $Sub = $this->addpagemodel->Sub();
            $navinfo = $this->addpagemodel->navLinks();
            $poly = $this->addpagemodel->Policy();

            $data = [
                'policy' => $poly,
                'navSub' => $Sub,
                'navinfo1' => $navinfo
            ];

            $this->view('addpages/policy', $data);
        }

    }
?>