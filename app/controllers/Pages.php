<?php

    class Pages extends Controller {

        public function __construct() {
            $this->mainModel = $this->model('main');
        }


        public function display() {

            $main= $this->mainModel->mainpost();
            $navinfo = $this->mainModel->navLinks();
            $Sub = $this->mainModel->Sub();

            $data = [
                'Main' => $main,
                'navSub' => $Sub,
                'navinfo1' => $navinfo

            ];

            $this->view('pages/display', $data);
        }

        public function led() 
        {
            $uname = $this->mainModel->leduname();
            $user = $this->mainModel->ledusercont();
            $down = $this->mainModel->leddownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->ledcontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->ledImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/led', $data);
        }

        public function sands() 
        {
            $uname = $this->mainModel->sandsuname();
            $user = $this->mainModel->sandsusercont();
            $down = $this->mainModel->sandsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->sandscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->sandsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/sands', $data);
        }


        public function Beginings() 
        {
            $uname = $this->mainModel->Beginingsuname();
            $user = $this->mainModel->Beginingsusercont();
            $down = $this->mainModel->Beginingsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->Beginingscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->BeginingsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/Beginings', $data);

        }

        //===========================addpage==============================
        
        public function addperson() {

            $Sub = $this->mainModel->Sub();
            $navinfo = $this->mainModel->navLinks();

            $data = [];

            if(!empty($_POST['fname']) && !empty($_POST['lname']) && !empty($_POST['uname']) && !empty($_POST['pass'])) {
                if($this->postModel->addPerson($_POST['fname'], $_POST['lname'], $_POST['uname'], $_POST['pass'])) {
                    $data = [
                        'navSub' => $Sub,
                        'navinfo1' => $navinfo,
                        "title" => "Thank you for adding in a user"
                    ];
                } else {
                    die('SOMETHING WENT WRONG!');
                }
            } else {
                $data = [
                    'navSub' => $Sub,
                    'navinfo1' => $navinfo,
                    "title" => "Please add a user"
                ];
            }

            $this->view('pages/addperson', $data);
        }

        public function Problems() 
        {
            $uname = $this->mainModel->problemsuname();
            $user = $this->mainModel->problemsusercont();
            $down = $this->mainModel->problemsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->problemscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->problemsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/Problems', $data);
        }

        public function cmodels() 
        {
            $uname = $this->mainModel->cmodelsuname();
            $user = $this->mainModel->cmodelsusercont();
            $down = $this->mainModel->cmodelsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->cmodelscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->cmodelsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/cmodels', $data);
        }

        public function dprints() 
        {
            $cont = $this->mainModel->printercontent();
            $uname = $this->mainModel->printeruname();
            $user = $this->mainModel->printerusercont();
            $down = $this->mainModel->printerdownload();
            $Sub = $this->mainModel->Sub();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->printerImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/dprints', $data);
        }


        public function code() 
        {
            $uname = $this->mainModel->codeuname();
            $user = $this->mainModel->codeusercont();
            $down = $this->mainModel->codedownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->codecontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->codeImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/code', $data);
        }

        public function design() 
        {
            $uname = $this->mainModel->designuname();
            $user = $this->mainModel->designusercont();
            $down = $this->mainModel->designdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->designcontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->designImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/design', $data);
        }


        public function mods() 
        {
            $uname = $this->mainModel->modsuname();
            $user = $this->mainModel->modsusercont();
            $down = $this->mainModel->modsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->modscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->modsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/mods', $data);
        }

        public function begining() 
        {
            $uname = $this->mainModel->begininguname();
            $user = $this->mainModel->beginingusercont();
            $down = $this->mainModel->beginingdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->beginingcontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->beginingImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/begining', $data);
        }

        public function racers() 
        {
            $uname = $this->mainModel->racersuname();
            $user = $this->mainModel->racersusercont();
            $down = $this->mainModel->racersdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->racerscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->racersImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/racers', $data);
        }

        public function wings() 
        {
            $uname = $this->mainModel->wingsuname();
            $user = $this->mainModel->wingsusercont();
            $down = $this->mainModel->wingsdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->wingscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->wingsImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/wings', $data);
        }

        public function aphoto() 
        {
            $uname = $this->mainModel->aphotouname();
            $user = $this->mainModel->aphotousercont();
            $down = $this->mainModel->aphotodownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->aphotocontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->aphotoImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/aphoto', $data);
        }


        public function fpv() 
        {
            $uname = $this->mainModel->fpvuname();
            $user = $this->mainModel->fpvusercont();
            $down = $this->mainModel->fpvdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->fpvcontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->fpvImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/fpv', $data);
        }

        public function minis() 
        {
            $uname = $this->mainModel->minisuname();
            $user = $this->mainModel->minisusercont();
            $down = $this->mainModel->minisdownload();
            $Sub = $this->mainModel->Sub();
            $cont = $this->mainModel->miniscontent();
            $navinfo = $this->mainModel->navLinks();
            $ImgCara = $this->mainModel->minisImgCara();

            $data = [
                'name' => $uname,
                'userinfo' => $user,
                'downloads' => $down,
                'Imgcara' => $ImgCara,
                'navinfo1' => $navinfo,
                'navSub' => $Sub,
                'content' => $cont,
            ];
            $this->view('pages/sub/minis', $data);
        }
    }
?>