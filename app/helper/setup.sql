USE containerdb;
DROP TABLE tbl_downloads;
DROP TABLE tbl_Content;
DROP TABLE tbl_imgtable;
DROP TABLE tbl_posts;
DROP TABLE tbl_user;
DROP TABLE tbl_subcat;
DROP TABLE tbl_cat;
DROP TABLE tbl_nav;
DROP TABLE tbl_addpage;

USE containerdb;
CREATE TABLE tbl_addpage (
    adpID int(255) NOT NULL AUTO_INCREMENT,
    adptitle VARCHAR(255),
    adpcont TEXT NOT NULL,
    adplink VARCHAR(255),
    adpimg VARCHAR(255),
    PRIMARY KEY (adpID)
) AUTO_INCREMENT =1;

USE containerdb;
CREATE TABLE tbl_nav (
    navID int(255) NOT NULL AUTO_INCREMENT,
    navName VARCHAR(255) NOT NULL,
    navLink VARCHAR(255) NOT NULL,
    PRIMARY KEY (navID)
)   AUTO_INCREMENT=1;

CREATE TABLE tbl_cat (
    CatID INT(255) NOT NULL AUTO_INCREMENT,
    CatDescription VARCHAR(255) NOT NULL,
    PRIMARY KEY (CatID)
) AUTO_INCREMENT =1;

USE containerdb;
CREATE TABLE tbl_subcat (
    SubcatID INT(255) NOT NULL AUTO_INCREMENT,
    CatID INT(255) NOT NULL,
    SubLink varchar(255) NOT NULL,
    Subimg varchar(255) NOT NULL,
    SubcatDescription VARCHAR(255) NOT NULL,
    PRIMARY KEY (SubcatID),
    FOREIGN KEY (CatID) REFERENCES tbl_cat(CatID)
) AUTO_INCREMENT =1;

USE containerdb;
CREATE TABLE tbl_user (
    userID INT(255) NOT NULL AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    LNAME VARCHAR(255),
    UNAME VARCHAR(255) NOT NULL,
    PASS VARCHAR(255) NOT NULL,
    POSTNO INT(255),
    COMENTSNO INT(255),
    PRIMARY KEY (userID)
) AUTO_INCREMENT=1;

USE containerdb;
CREATE TABLE tbl_posts (

    POSTID INT(255) NOT NULL AUTO_INCREMENT,
    userID INT(255) NOT NULL,
    CatID INT(255) NOT NULL,
    PNAME VARCHAR(255) NOT NULL,
    PDescription VARCHAR(255),
    PRIMARY KEY (POSTID),
    FOREIGN KEY (userID) REFERENCES tbl_user(userID),
    FOREIGN KEY (CatID) REFERENCES tbl_cat(CatID)
) AUTO_INCREMENT=1;

USE containerdb;
CREATE TABLE tbl_imgtable (
    IMGID INT(255) NOT NULL AUTO_INCREMENT,
    IMG VARCHAR(255),
    POSTID INT(255) NOT NULL,
    PRIMARY KEY (IMGID),
    FOREIGN KEY (POSTID) REFERENCES tbl_posts(POSTID)
) AUTO_INCREMENT=1;

USE containerdb;
CREATE TABLE tbl_Content (
    ContID INT(255) NOT NULL AUTO_INCREMENT,
    Heading VARCHAR(255),
    Content TEXT,
    POSTID INT(255) NOT NULL,
    FOREIGN KEY (POSTID) REFERENCES tbl_posts(POSTID),
    PRIMARY KEY (ContID)
) AUTO_INCREMENT=1;

USE containerdb;
CREATE TABLE tbl_downloads (
    DID INT(255) NOT NULL AUTO_INCREMENT,
    POSTID INT(255),
    dNAME VARCHAR(255) NOT NULL,
    dLink VARCHAR(255) NOT NULL,
    PRIMARY KEY (DID),
    FOREIGN KEY (POSTID) REFERENCES tbl_posts(POSTID)
) AUTO_INCREMENT=1;

USE containerdb;
INSERT into tbl_addpage (adptitle, adpcont, adpimg) VALUES ('About Us', 
'There was no possibility of taking a walk that day. We
had been wandering, indeed, in the leafless shrubbery an
hour in the morning; but since dinner (Mrs. Reed, when
there was no company, dined early) the cold winter wind
had brought with it clouds so sombre, and a rain so
penetrating, that further out-door exercise was now out of
the question.
I was glad of it: I never liked long walks, especially on
chilly afternoons: dreadful to me was the coming home in
the raw twilight, with nipped fingers and toes, and a heart
saddened by the chidings of Bessie, the nurse, and
humbled by the consciousness of my physical inferiority to
Eliza, John, and Georgiana Reed.
The said Eliza, John, and Georgiana were now
clustered round their mama in the drawing-room: she lay
reclined on a sofa by the fireside, and with her darlings
about her (for the time neither quarrelling nor crying)
looked perfectly happy. Me, she had dispensed from
joining the group; saying, ‘She regretted to be under the
necessity of keeping me at a distance; but that until she
heard from Bessie, and could discover by her own
observation, that I was endeavouring in good earnest to
acquire a more sociable and childlike disposition, a more
attractive and sprightly manner— something lighter,
franker, more natural, as it were—she really must exclude
me from privileges intended only for contented, happy,
little children.’', 'images/p(about).jpg');
INSERT into tbl_addpage (adptitle, adpcont, adpimg) VALUES ('FAQ', 
'Q) I cannot tell what sentiment haunted the quite solitary
churchyard, with its inscribed headstone; its gate, its two
trees, its low horizon, girdled by a broken wall, and its
newly-risen crescent, attesting the hour of eventide.
The two ships becalmed on a torpid sea, I believed to
be marine phantoms.

A) The fiend pinning down the thief’s pack behind him, I
passed over quickly: it was an object of terror.

Q) So was the black horned thing seated aloof on a rock,
surveying a distant crowd surrounding a gallows.

A) Each picture told a story; mysterious often to my
undeveloped understanding and imperfect feelings, yet
ever profoundly interesting: as interesting as the tales

Q) Bessie sometimes narrated on winter evenings, when she
chanced to be in good humour; and when, having
brought her ironing-table to the nursery hearth, 

A) she allowed us to sit about it, and while she got up Mrs.
Reed’s lace frills, and crimped her nightcap borders, fed
our eager attention with passages of love and adventure

Q) taken from old fairy tales and other ballads; or (as at a later
period I discovered) from the pages of Pamela, and Henry,
Earl of Moreland.

A) Jane Eyre is in public domain', 'images/p(faq).JPG');
INSERT into tbl_addpage (adptitle, adpcont, adpimg) VALUES ('External Links', 'Some fun Links', 'images/p(external).jpg');
INSERT into tbl_addpage (adpcont, adplink) VALUES ('Thingiverse', 'www.thingiverse.com');
INSERT into tbl_addpage (adpcont, adplink) VALUES ('instuctables', 'www.instructables.com');
INSERT into tbl_addpage (adpcont, adplink) VALUES ('Google', 'www.google.com');
INSERT into tbl_addpage (adpcont, adplink) VALUES ('bitbucket', 'bitbucket.org');
INSERT into tbl_addpage (adpcont, adplink) VALUES ('reddit', 'www.reddit.com');
INSERT into tbl_addpage (adptitle, adpcont, adpimg) VALUES ('Policy Documents', 
'terror he inspired, because I had no appeal whatever
against either his menaces or his inflictions; the servants
did not like to offend their young master by taking my
part against him, and Mrs. Reed was blind and deaf on the
subject: she never saw him strike or heard him abuse me,
though he did both now and then in her very presence,
more frequently, however, behind her back.
Habitually obedient to John, I came up to his chair: he
spent some three minutes in thrusting out his tongue at
me as far as he could without damaging the roots: I knew
he would soon strike, and while dreading the blow, I
mused on the disgusting and ugly appearance of him who
would presently deal it. I wonder if he read that notion in
my face; for, all at once, without speaking, he struck
suddenly and strongly. I tottered, and on regaining my
equilibrium retired back a step or two from his chair.
‘That is for your impudence in answering mama awhile
since,’ said he, ‘and for your sneaking way of getting
behind curtains, and for the look you had in your eyes
two minutes since, you rat!’
Accustomed to John Reed’s abuse, I never had an idea
of replying to it; my care was how to endure the blow
which would certainly follow the insult.
‘What were you doing behind the curtain?’ he asked.', 'images/p(policy).jpg');

USE containerdb;
INSERT into tbl_nav (navName, navLink) VALUES ('3D Printing', 'pages/Dprint' );
INSERT into tbl_nav (navName, navLink) VALUES ('RC Planes', 'pages/RCplanes' );
INSERT into tbl_nav (navName, navLink) VALUES ('RC Quads', 'pages/RCQuads' );
INSERT into tbl_nav (navName, navLink) VALUES ('Arduino', 'pages/Arduino' );
INSERT into tbl_nav (navName, navLink) VALUES ('Robotics', 'pages/Robotics' );
INSERT into tbl_nav (navName, navLink) VALUES ('Home page', 'pages/display' );
INSERT into tbl_nav (navName, navLink) VALUES ('About Us', 'addpages/about' );
INSERT into tbl_nav (navName, navLink) VALUES ('FAQ Page', 'addpages/faq' );
INSERT into tbl_nav (navName, navLink) VALUES ('External', 'addpages/External' );
INSERT into tbl_nav (navName, navLink) VALUES ('Admin Aplication', 'pages/' );
INSERT into tbl_nav (navName, navLink) VALUES ('Policy Documents', 'addpages/policy' );

USE containerdb;
INSERT INTO tbl_cat (CatDescription) VALUES ('3D Printing');
INSERT INTO tbl_cat (CatDescription) VALUES ('RC Planes');
INSERT INTO tbl_cat (CatDescription) VALUES ('RC Quads');
INSERT INTO tbl_cat (CatDescription) VALUES ('Arduino');
INSERT INTO tbl_cat (CatDescription) VALUES ('Robotics');

USE containerdb;
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('1', 'pages/cmodels', 'Chinese Models', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('1', 'pages/dprints', '3D Prints', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('1', 'pages/Problems', 'Problems', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('2', 'pages/fpv', 'FPV', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('2', 'pages/wings', 'Wings', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('2', 'pages/beginings', 'Beginings', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('3', 'pages/minis', 'Minis', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('3', 'pages/racers', 'Racers', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('3', 'pages/aphoto', 'Aerial Photograpy', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('4', 'pages/begining', 'Begining', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('4', 'pages/led', "LED's", 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('4', 'pages/sands', 'Servos & Sensors', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('5', 'pages/design', 'Design', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('5', 'pages/code', 'Code', 'images/p(1).JPG');
INSERT INTO tbl_subcat (CatID, SubLink, SubcatDescription, Subimg) VALUES ('5', 'pages/mods', 'Mods', 'images/p(1).JPG');

USE containerdb;
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Robert', 'Baratheon', 'Piguy', 'pleasegive', 3, 10);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Jaime', 'Lannister', 'nohand', 'ME', 1, 0);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Catelyn', 'Stark', 'seeing-red', 'good',2 , 3);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Daenerys', 'Targaryen', 'blondequeen', 'grades',3 , 5);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Cersei', 'Lannister', 'ruleingishard', 'it',2 , 0);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Viserys', 'Targaryen', 'Ilikemyhelmit', 'would',1 , 6);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Robb', 'Stark', 'hitatree', 'make', 1, 4);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Arya', 'Stark', 'peekaboo', 'me',1 , 7);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Theon', 'Greyjoy', 'nononoarea', 'happy', 1, 1);
INSERT INTO tbl_user (FNAME,LNAME, UNAME,PASS, POSTNO, COMENTSNO) VALUES ('Jon', 'Snow', 'spoiler', 'Thankyou',1 , 0);

USE containerdb;
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('1', '1', 'The Redkeep', 
'Those flames only appear on one night, and on that night no
man of this land will, if he can help it, stir without his doors.');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('2', '1', 'The Chum Bucket', 
'And, dear sir, even if he did he would not know what to do.
Why, even the peasant that you tell me of who marked the');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('3', '1', 'Hello', 
'place of the flame would not know where to look in daylight
even for his own work. Even you would not, I dare be sworn,
be able to find these places again?’');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('4', '2', 'This IS', 
'‘There you are right,’ I said. ‘I know no more than the
dead where even to look for them.’ Then we drifted into oth-
er matters.');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('5', '2',  'A post', 
'Come,’ he said at last, ‘tell me of London and of the
house which you have procured for me.’ With an apolo-');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('6', '2',  'OF test', 
'gy for my remissness, I went into my own room to get the
papers from my bag. Whilst I was placing them in order I
heard a rattling of china and silver in the next room, and');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('7', '3', 'Data', 
'as I passed through, noticed that the table had been cleared
and the lamp lit, for it was by this time deep into the dark.
The lamps were also lit in the study or library, and I found');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('8', '3',  'And there', 
'the Count lying on the sofa, reading, of all things in the
world, an English Bradshaw’s Guide. When I came in he');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('9', '3',  'Is no', 
'cleared the books and papers from the table, and with him
I went into plans and deeds and figures of all sorts. He was
interested in everything, and asked me a myriad question');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('10', '4',  'Chance',
 'about the place and its surroundings. He clearly had studied
beforehand all he could get on the subject of the neighbour');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('1', '4', 'That',
 'hood, for he evidently at the end knew very much more
than I did. When I remarked this, he answered.
‘Well, but, my friend, is it not needful that I should?
When I go there I shall be all alone, and my friend Harker');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('1', '4', 'I Pass', 
'Jonathan, nay, pardon me. I fall into my country’s habit of
putting your patronymic first, my friend Jonathan Harker
will not be by my side to correct and aid me. He will be in');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('3', '5', 'Corse', 
'Exeter, miles away, probably working at papers of the law
with my other friend, Peter Hawkins. So!’');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('4', '5',  'So I Will',
 'We went thoroughly into the business of the purchase
of the estate at Purfleet. When I had told him the facts and');
INSERT INTO tbl_posts (userID, CatID, PNAME, PDescription) VALUES ('5', '5', 'Repeat it',
 'got his signature to the necessary papers, and had written
a letter with them ready to post to Mr. Hawkins, he began');

USE containerdb;
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(1).JPG','1');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(2).JPG','1');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(3).JPG','1');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(1).JPG','2');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(3).JPG','2');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(5).JPG','3');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(12).JPG','4');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(16).JPG','4');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(9).JPG','4');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(4).JPG','5');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(10).JPG','6');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(11).JPG','6');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(2).JPG','7');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(5).JPG','7');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(8).JPG','7');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(14).JPG','8');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(17).JPG','8');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(1).JPG','9');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(4).JPG','9');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(8).JPG','9');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(2).JPG','9');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(18).JPG','10');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(6).JPG','10');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(18).JPG','11');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(6).JPG','11');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(9).JPG','11');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(9).JPG','11');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(12).JPG','12');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(16).JPG','12');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(9).JPG','13');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(4).JPG','13');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(10).JPG','13');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(15).JPG','14');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(2).JPG','15');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(5).JPG','15');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(8).JPG','15');
INSERT INTO tbl_imgtable (IMG, POSTID) VALUES ('images/p(14).JPG','15');

USE containerdb;
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Start the book', 
'Jonathan Harker’s Journal Continued
5 May.—I must have been asleep, for certainly if I had
been fully awake I must have noticed the approach of such
a remarkable place. In the gloom the courtyard looked of
considerable size, and as several dark ways led from it under
great round arches, it perhaps seemed bigger than it really
is. I have not yet been able to see it by daylight.
When the caleche stopped, the driver jumped down
and held out his hand to assist me to alight. Again I could
not but notice his prodigious strength. His hand actually
seemed like a steel vice that could have crushed mine if he
had chosen. Then he took my traps, and placed them on the
ground beside me as I stood close to a great door, old and
studded with large iron nails, and set in a projecting door-
way of massive stone.', 1);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('discect the myth', 
'I could see even in the dim light that
the stone was massively carved, but that the carving had
been much worn by time and weather. As I stood, the driver
jumped again into his seat and shook the reins. The horses
started forward, and trap and all disappeared down one of
the dark openings.
I stood in silence where I was, for I did not know what
to do. Of bell or knocker there was no sign. Through these
frowning walls and dark window openings it was not likely
that my voice could penetrate. The time I waited seemed', 1);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('feed the filament', 
'endless, and I felt doubts and fears crowding upon me.
What sort of place had I come to, and among what kind of
people? What sort of grim adventure was it on which I had
embarked? Was this a customary incident in the life of a
solicitor’s clerk sent out to explain the purchase of a London
estate to a foreigner? Solicitor’s clerk! Mina would not like
that. Solicitor, for just before leaving London I got word that
my examination was successful, and I am now a full-blown
solicitor! I began to rub my eyes and pinch myself to see
if I were awake. It all seemed like a horrible nightmare to
me, and I expected that I should suddenly awake, and find
myself at home, with the dawn struggling in through the
windows, as I had now and again felt in the morning after
a day of overwork. But my flesh answered the pinching test,
and my eyes were not to be deceived. I was indeed awake
and among the Carpathians. All I could do now was to be
patient, and to wait the coming of morning.
Just as I had come to this conclusion I heard a heavy
step approaching behind the great door, and saw through
the chinks the gleam of a coming light.', 2);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Solving Printlines', 
'The old man motioned me in with his right hand
with a courtly gesture, saying in excellent English, but with
a strange intonation.
‘Welcome to my house! Enter freely and of your own free
will!’ He made no motion of stepping to meet me, but stood
like a statue, as though his gesture of welcome had fixed
him into stone. The instant, however, that I had stepped
over the threshold, he moved impulsively forward, and
holding out his hand grasped mine with a strength which
made me wince, an effect which was not lessened by the fact
that it seemed cold as ice, more like the hand of a dead than
a living man. Again he said.', 3);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('New Controler', 
'‘Welcome to my house! Enter freely. Go safely, and leave
something of the happiness you bring!’ The strength of the
handshake was so much akin to that which I had noticed
in the driver, whose face I had not seen, that for a moment
I doubted if it were not the same person to whom I was
speaking. So to make sure, I said interrogatively, ‘Count
Dracula?’
He bowed in a courtly way as he replied, ‘I am Dracula,
and I bid you welcome, Mr. Harker, to my house. Come in,
the night air is chill, and you must need to eat and rest.’ As
he was speaking, he put the lamp on a bracket on the wall,
and stepping out, took my luggage. He had carried it in be-
fore I could forestall him. I protested, but he insisted.
‘Nay, sir, you are my guest. It is late, and my people are
not available. Let me see to your comfort myself.’ He in-
sisted on carrying my traps along the passage, and then up
a great winding stair, and along another great passage, on', 3);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('5.8ghz is the best', 
'At the end of this
he threw open a heavy door, and I rejoiced to see within a
well-lit room in which a table was spread for supper, and on
whose mighty hearth a great fire of logs, freshly replenished,
flamed and flared.
The Count halted, putting down my bags, closed the door,
and crossing the room, opened another door, which led into
a small octagonal room lit by a single lamp, and seeming-
ly without a window of any sort. Passing through this, he
opened another door, and motioned me to enter. It was a
welcome sight. For here was a great bedroom well lighted
and warmed with another log fire, also added to but late-
ly, for the top logs were fresh, which sent a hollow roar up
the wide chimney. The Count himself left my luggage inside
and withdrew, saying, before he closed the door.
‘You will need, after your journey, to refresh yourself by
making your toilet. I trust you will find all you wish. When
you are ready, come into the other room, where you will
find your supper prepared.’
The light and warmth and the Count’s courteous wel-
come seemed to have dissipated all my doubts and fears.
Having then reached my normal state, I discovered that I
was half famished with hunger. So making a hasty toilet, I
went into the other room.', 4);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Profiling the Wings', 
'I found supper already laid out. My host, who stood on
one side of the great fireplace, leaning against the stonework,
made a graceful wave of his hand to the table, and said,
‘I pray you, be seated and sup how you please. You will I
trust, excuse me that I do not join you, but I have dined already,
and I do not sup.’', 5);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Starting tips', 
'I handed to him the sealed letter which Mr. Hawkins
had entrusted to me. He opened it and read it gravely. Then,
with a charming smile, he handed it to me to read. One pas-
sage of it, at least, gave me a thrill of pleasure.
‘I must regret that an attack of gout, from which malady
I am a constant sufferer, forbids absolutely any travelling on
my part for some time to come. But I am happy to say I can
send a sufficient substitute, one in whom I have every possi-
ble confidence. He is a young man, full of energy and talent
in his own way, and of a very faithful disposition. He is dis-
creet and silent, and has grown into manhood in my service.
He shall be ready to attend on you when you will during his
stay, and shall take your instructions in all matters.’
The count himself came forward and took off the cover
of a dish, and I fell to at once on an excellent roast chicken.
This, with some cheese and a salad and a bottle of old tokay,
of which I had two glasses, was my supper. During the time
I was eating it the Count asked me many questions as to my
journey, and I told him by degrees all I had experienced.', 6);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Cut the Frame', 
'By this time I had finished my supper, and by my host’s
desire had drawn up a chair by the fire and begun to smoke
a cigar which he offered me, at the same time excusing him-
self that he did not smoke. I had now an opportunity of
observing him, and found him of a very marked physiognomy.
His face was a strong, a very strong, aquiline, with high
bridge of the thin nose and peculiarly arched nostrils, with
lofty domed forehead, and hair growing scantily round the
temples but profusely elsewhere. His eyebrows were very
massive, almost meeting over the nose, and with bushy hair
that seemed to curl in its own profusion. The mouth, so
far as I could see it under the heavy moustache, was fixed
and rather cruel-looking, with peculiarly sharp white teeth.
These protruded over the lips, whose remarkable ruddiness
showed astonishing vitality in a man of his years. For the
rest, his ears were pale, and at the tops extremely pointed.
The chin was broad and strong, and the cheeks firm though
thin. The general effect was one of extraordinary pallor.', 7);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Weight maters', 
'temples but profusely elsewhere. His eyebrows were very
massive, almost meeting over the nose, and with bushy hair
that seemed to curl in its own profusion. The mouth, so
far as I could see it under the heavy moustache, was fixed
and rather cruel-looking, with peculiarly sharp white teeth.
These protruded over the lips, whose remarkable ruddiness
showed astonishing vitality in a man of his years. For the
rest, his ears were pale, and at the tops extremely pointed.
The chin was broad and strong, and the cheeks firm though
thin. The general effect was one of extraordinary pallor.', 8);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Saving the grams', 
'Hitherto I had noticed the backs of his hands as they lay
on his knees in the firelight, and they had seemed rather
white and fine. But seeing them now close to me, I could
not but notice that they were rather coarse, broad, with
squat fingers. Strange to say, there were hairs in the centre
of the palm. The nails were long and fine, and cut to a sharp
point. As the Count leaned over me and his hands touched
me, I could not repress a shudder. It may have been that his
breath was rank, but a horrible feeling of nausea came over
me, which, do what I would, I could not conceal.', 8);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('To get that steady image', 
'The Count, evidently noticing it, drew back. And with a
grim sort of smile, which showed more than he had yet done
his protruberant teeth, sat himself down again on his own
side of the fireplace. We were both silent for a while, and
as I looked towards the window I saw the first dim streak
of the coming dawn. There seemed a strange stillness over
everything. But as I listened, I heard as if from down below
in the valley the howling of many wolves. The Count’s eyes
gleamed, and he said.', 9);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Panning the rightway', 
'‘Listen to them, the children of the night. What music
they make!’ Seeing, I suppose, some expression in my face
strange to him, he added, ‘Ah, sir, you dwellers in the city
cannot enter into the feelings of the hunter.’ Then he rose
and said.
‘But you must be tired. Your bedroom is all ready, and
tomorrow you shall sleep as late as you will. I have to be
away till the afternoon, so sleep well and dream well!’ With
a courteous bow, he opened for me himself the door to the
octagonal room, and I entered my bedroom.
I am all in a sea of wonders. I doubt. I fear. I think strange
things, which I dare not confess to my own soul. God keep
me, if only for the sake of those dear to me!', 9);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Knight rider', 
'7 May.—It is again early morning, but I have rested and
enjoyed the last twenty-four hours. I slept till late in the day,
and awoke of my own accord. When I had dressed myself
I went into the room where we had supped, and found a
cold breakfast laid out, with coffee kept hot by the pot be-
ing placed on the hearth. There was a card on the table, on
which was written—‘I have to be absent for a while. Do not
wait for me. D.’ I set to and enjoyed a hearty meal. When
I had done, I looked for a bell, so that I might let the ser-
vants know I had finished, but I could not find one. There
are certainly odd deficiencies in the house, considering the
extraordinary evidences of wealth which are round me. The
table service is of gold, and so beautifully wrought that it
must be of immense value. The curtains and upholstery of
the chairs and sofas and the hangings of my bed are of the
costliest and most beautiful fabrics, and must have been of', 10);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('The 4x4 box', 
'fabulous value when they were made, for they are centuries
old, though in excellent order. I saw something like them in
Hampton Court, but they were worn and frayed and moth-
eaten. But still in none of the rooms is there a mirror. There
is not even a toilet glass on my table, and I had to get the
little shaving glass from my bag before I could either shave
or brush my hair. I have not yet seen a servant anywhere, or
heard a sound near the castle except the howling of wolves.
Some time after I had finished my meal, I do not know
whether to call it breakfast or dinner, for it was between five
and six o’clock when I had it, I looked about for something
to read, for I did not like to go about the castle until I had
asked the Count’s permission. There was absolutely nothing
in the room, book, newspaper, or even writing materials, so
I opened another door in the room and found a sort of li-
brary. The door opposite mine I tried, but found locked.
In the library I found, to my great delight, a vast num-
ber of English books, whole shelves full of them, and bound
volumes of magazines and newspapers. A table in the cen-
tre was littered with English magazines and newspapers,
though none of them were of very recent date. The books
were of the most varied kind, history, geography, politics,
political economy, botany, geology, law, all relating to Eng-
land and English life and customs and manners. There were
even such books of reference as the London Directory, the
‘Red’ and ‘Blue’ books, Whitaker’s Almanac, the Army and
Navy Lists, and it somehow gladdened my heart to see it,
the Law List.', 11);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('RBG Controler', 
'Whilst I was looking at the books, the door opened, and
the Count entered. He saluted me in a hearty way, and
hoped that I had had a good night’s rest. Then he went on.
‘I am glad you found your way in here, for I am sure there
is much that will interest you. These companions,’ and he
laid his hand on some of the books, ‘have been good friends
to me, and for some years past, ever since I had the idea
of going to London, have given me many, many hours of
pleasure. Through them I have come to know your great
England, and to know her is to love her. I long to go through
the crowded streets of your mighty London, to be in the
midst of the whirl and rush of humanity, to share its life, its
change, its death, and all that makes it what it is. But alas!
As yet I only know your tongue through books. To you, my
friend, I look that I know it to speak.’', 11);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Legs', 
'He saluted me in a hearty way, and
hoped that I had had a good night’s rest. Then he went on.
‘I am glad you found your way in here, for I am sure there
is much that will interest you. These companions,’ and he
laid his hand on some of the books, ‘have been good friends
to me, and for some years past, ever since I had the idea
of going to London, have given me many, many hours of
pleasure. Through them I have come to know your great
England, and to know her is to love her. I long to go through
the crowded streets of your mighty London, to be in the
midst of the whirl and rush of humanity, to share its life, its
change, its death, and all that makes it what it is. But alas!
As yet I only know your tongue through books. To you, my
friend, I look that I know it to speak.’
‘But, Count,’ I said, ‘You know and speak English thor-
oughly!’ He bowed gravely.', 12);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Bipeds', 
'‘I thank you, my friend, for your all too-flattering esti-
mate, but yet I fear that I am but a little way on the road I
would travel. True, I know the grammar and the words, but
yet I know not how to speak them.’
‘Indeed,’ I said, ‘You speak excellently.’
‘Not so,’ he answered. ‘Well, I know that, did I move and
speak in your London, none there are who would not know
me for a stranger. That is not enough for me. Here I am no-
ble. I am a Boyar. The common people know me, and I am
master. But a stranger in a strange land, he is no one. Men
know him not, and to know not is to care not for. I am con-
tent if I am like the rest, so that no man stops if he sees me,
or pauses in his speaking if he hears my words, ‘Ha, ha! A', 13);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('C#', 
'stranger!’ I have been so long master that I would be master
still, or at least that none other should be master of me. You
come to me not alone as agent of my friend Peter Hawkins,
of Exeter, to tell me all about my new estate in London. You
shall, I trust, rest here with me a while, so that by our talk-
ing I may learn the English intonation. And I would that
you tell me when I make error, even of the smallest, in my
speaking. I am sorry that I had to be away so long today, but
you will, I know forgive one who has so many important af-
fairs in hand.’', 14);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Java', 
'Of course I said all I could about being willing, and asked
if I might come into that room when I chose. He answered,
‘Yes, certainly,’ and added.
‘You may go anywhere you wish in the castle, except
where the doors are locked, where of course you will not
wish to go. There is reason that all things are as they are,
and did you see with my eyes and know with my knowledge,
you would perhaps better understand.’ I said I was sure of
this, and then he went on.
‘We are in Transylvania, and Transylvania is not Eng-
land. Our ways are not your ways, and there shall be to you
many strange things. Nay, from what you have told me of
your experiences already, you know something of what
strange things there may be.’', 14);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Python', 
'This led to much conversation, and as it was evident
that he wanted to talk, if only for talking’s sake, I asked
him many questions regarding things that had already
happened to me or come within my notice. Sometimes he
sheered off the subject, or turned the conversation by pre-', 14);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Weapons', 
'I asked most frankly. Then as time went on, and I had got
somewhat bolder, I asked him of some of the strange things
of the preceding night, as for instance, why the coachman
went to the places where he had seen the blue flames. He
then explained to me that it was commonly believed that on
a certain night of the year, last night, in fact, when all evil
spirits are supposed to have unchecked sway, a blue flame is
seen over any place where treasure has been concealed.
‘That treasure has been hidden,’ he went on, ‘in the re-
gion through which you came last night, there can be but
little doubt. For it was the ground fought over for centuries
by the Wallachian, the Saxon, and the Turk. Why, there is
hardly a foot of soil in all this region that has not been en-
riched by the blood of men, patriots or invaders. In the old
days there were stirring times, when the Austrian and the
Hungarian came up in hordes, and the patriots went out
to meet them, men and women, the aged and the children
too, and waited their coming on the rocks above the pass-
es, that they might sweep destruction on them with their
artificial avalanches.', 15);
INSERT INTO tbl_Content (Heading, Content, POSTID) VALUES ('Wamin', 
'‘But how,’ said I, ‘can it have remained so long undiscov-
ered, when there is a sure index to it if men will but take
the trouble to look? ‘The Count smiled, and as his lips ran
back over his gums, the long, sharp, canine teeth showed
out strangely. He answered.
‘Because your peasant is at heart a coward and a fool!', 15);

USE containerdb;
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (1, 'fan.stl', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (1, 'cover.stl', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (2, 'Frame.stl', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (2, 'cover-fro.stl', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (3, 'Hello.text', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (3, 'canYounot', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (3, 'gethis', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (4, 'coverplate', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (5, 'second network', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (5, 'sword', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (6, 'game', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (7, 'staff', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (7, 'getcode', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (7, 'car', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (8, 'dolphin', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (9, 'raydiagram', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (9, 'halo', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (10, 'rail', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (10, 'god', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (10, 'telescope', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (10, 'crown', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (11, 'virus', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (11, 'alternatior', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (12, 'petrol', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (13, 'sidemirrors', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (13, 'windsreenwipers', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (14, 'tires', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (15, 'failer', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (15, 'captions', '');
INSERT INTO tbl_downloads (POSTID, dNAME, dLink) VALUES (15, 'servocheck', '');


use containerdb;
