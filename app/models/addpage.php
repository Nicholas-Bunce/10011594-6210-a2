<?php

    class addpage {

        private $db;

         public function __construct() {
            $this->db = new Database;
        } 

        public function Sub() {
            $this->db->query("SELECT * FROM tbl_subcat");
            return $this->db->resultSet();
        }

        public function PSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 1");
            return $this->db->resultSet();
        }
        public function RPSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 2");
            return $this->db->resultSet();
        }

        public function RQSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 3");
            return $this->db->resultSet();
        }

        public function ASub() {
            $this->db->query("SELECT * FROM tbl_subcat WHERE CatID = 4");
            return $this->db->resultSet();
        }
        public function RSub() {
            $this->db->query("SELECT * FROM tbl_subcat WHERE CatID = 5");
            return $this->db->resultSet();
        }


        public function SubCat() {
            $this->db->query("SELECT s.SubcatID, s.SubcatDescription, s.CatID, c.CatDescription
            from tbl_subcat s
            LEFT OUTER JOIN tbl_cat c on s.CatID=c.CatID");
            return $this->db->resultSet();
        }

        public function navLinks() {
            $this->db->query("SELECT * FROM tbl_nav");
            return $this->db->resultSet();
        }

        public function about() {
            $this->db->query("SELECT * FROM tbl_addpage where adpID = 1");
            return $this->db->resultSet();
        }

        public function FAQ() {
            $this->db->query("SELECT * FROM tbl_addpage where adpID = 2");
            return $this->db->resultSet();
        }

        public function External() {
            $this->db->query("SELECT * FROM tbl_addpage where adpID = 3");
            return $this->db->resultSet();
        }

        public function Policy() {
            $this->db->query("SELECT * FROM tbl_addpage where adpID = 9");
            return $this->db->resultSet();
        }
    
    }
?>