<?php

    class main {

        private $db;

         public function __construct() {
            $this->db = new Database;
        } 

        public function Sub() {
            $this->db->query("SELECT * FROM tbl_subcat");
            return $this->db->resultSet();
        }

        public function PSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 1");
            return $this->db->resultSet();
        }
        public function RPSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 2");
            return $this->db->resultSet();
        }

        public function RQSub() {
            $this->db->query("SELECT * FROM tbl_subcat  WHERE CatID = 3");
            return $this->db->resultSet();
        }

        public function ASub() {
            $this->db->query("SELECT * FROM tbl_subcat WHERE CatID = 4");
            return $this->db->resultSet();
        }
        public function RSub() {
            $this->db->query("SELECT * FROM tbl_subcat WHERE CatID = 5");
            return $this->db->resultSet();
        }


        public function SubCat() {
            $this->db->query("SELECT s.SubcatID, s.SubcatDescription, s.CatID, c.CatDescription
            from tbl_subcat s
            LEFT OUTER JOIN tbl_cat c on s.CatID=c.CatID");
            return $this->db->resultSet();
        }

        public function navLinks() {
            $this->db->query("SELECT * FROM tbl_nav");
            return $this->db->resultSet();
        }
        
        public function UserInfo() {
            $this->db->query("select p.POSTID, p.userID, p.CatID, p.PNAME, u.UNAME, u.userID, u.UNAME, u.POSTNO, u.COMENTSNO
            FROM tbl_posts p
            LEFT OUTER JOIN tbl_cat c on p.CatID=c.CatID
            LEFT OUTER JOIN tbl_imgtable i on p.POSTID=i.POSTID
            LEFT OUTER JOIN tbl_user u on p.userID=u.userID");
            return $this->db->resultSet();
        }
//===========================homepage================================        
        public function mainpost() {
            $this->db->query("SELECT p.POSTID, p.userID, p.CatID, p.PNAME, p.PDescription, i.IMG, c.CatDescription
            FROM tbl_posts p
            LEFT OUTER JOIN tbl_cat c on p.CatID=c.CatID
            LEFT OUTER JOIN tbl_imgtable i on p.POSTID=i.POSTID
            LEFT OUTER JOIN tbl_user u on p.userID=u.userID");
            return $this->db->resultSet();
        }
    
// ==============================addpages=====================================


        public function addPerson($fn, $ln) {

            $this->db->query("INSERT INTO tbl_user (FNAME, LNAME, UNAME, PASS) VALUES (:fn, :ln, :un, :p)");

            $this->db->bind(":fn", $fn);
            $this->db->bind(":ln", $ln);
            $this->db->bind(":un", $un);
            $this->db->bind(":p", $p);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

         //=========================Beginings======================================================

         public function Beginingscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 4");
            return $this->db->resultSet();
        }

        public function Beginingsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 4");
            return $this->db->resultSet();
        }

        public function Beginingsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 4");
            return $this->db->resultSet();
        }

        public function BeginingsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 4");
            return $this->db->resultSet();
        }

        public function Beginingsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 4");
            return $this->db->resultSet();
        }


        //=========================led======================================================

        public function ledcontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 9");
            return $this->db->resultSet();
        }

        public function leduname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 9");
            return $this->db->resultSet();
        }

        public function ledusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 9");
            return $this->db->resultSet();
        }

        public function ledImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 9");
            return $this->db->resultSet();
        }

        public function leddownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 9");
            return $this->db->resultSet();
        }

        //=========================sands=====================================================

        public function sandscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 14");
            return $this->db->resultSet();
        }

        public function sandsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 14");
            return $this->db->resultSet();
        }

        public function sandsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 14");
            return $this->db->resultSet();
        }

        public function sandsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 14");
            return $this->db->resultSet();
        }

        public function sandsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 14");
            return $this->db->resultSet();
        }

        public function printercontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 1");
            return $this->db->resultSet();
        }

        public function printeruname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 1");
            return $this->db->resultSet();
        }

        public function printerusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 1");
            return $this->db->resultSet();
        }

        public function printerImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 1");
            return $this->db->resultSet();
        }

        public function printerdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 1");
            return $this->db->resultSet();
        }

        //=========================cmodels======================================================

        public function cmodelscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 5");
            return $this->db->resultSet();
        }

        public function cmodelsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 5");
            return $this->db->resultSet();
        }

        public function cmodelsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 5");
            return $this->db->resultSet();
        }

        public function cmodelsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 5");
            return $this->db->resultSet();
        }

        public function cmodelsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 5");
            return $this->db->resultSet();
        }

         //=========================problems======================================================

         public function problemscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 12");
            return $this->db->resultSet();
        }

        public function problemsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 12");
            return $this->db->resultSet();
        }

        public function problemsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 12");
            return $this->db->resultSet();
        }

        public function problemsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 12");
            return $this->db->resultSet();
        }

        public function problemsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 12");
            return $this->db->resultSet();
        }

        public function aphotocontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 2");
            return $this->db->resultSet();
        }

        public function aphotouname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 2");
            return $this->db->resultSet();
        }

        public function aphotousercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 2");
            return $this->db->resultSet();
        }

        public function aphotoImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 2");
            return $this->db->resultSet();
        }

        public function aphotodownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 2");
            return $this->db->resultSet();
        }

         //=========================fpv======================================================

         public function fpvcontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 8");
            return $this->db->resultSet();
        }

        public function fpvuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 8");
            return $this->db->resultSet();
        }

        public function fpvusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 8");
            return $this->db->resultSet();
        }

        public function fpvImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 8");
            return $this->db->resultSet();
        }

        public function fpvdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 8");
            return $this->db->resultSet();
        }
        
        //=========================minis======================================================

        public function miniscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 10");
            return $this->db->resultSet();
        }

        public function minisuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 10");
            return $this->db->resultSet();
        }

        public function minisusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 10");
            return $this->db->resultSet();
        }

        public function minisImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 10");
            return $this->db->resultSet();
        }

        public function minisdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 10");
            return $this->db->resultSet();
        }

        public function beginingcontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 3");
            return $this->db->resultSet();
        }

        public function begininguname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 3");
            return $this->db->resultSet();
        }

        public function beginingusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 3");
            return $this->db->resultSet();
        }

        public function beginingImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 3");
            return $this->db->resultSet();
        }

        public function beginingdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 3");
            return $this->db->resultSet();
        }

        //=========================racers======================================================

        public function racerscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 13");
            return $this->db->resultSet();
        }

        public function racersuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 13");
            return $this->db->resultSet();
        }

        public function racersusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 13");
            return $this->db->resultSet();
        }

        public function racersImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 13");
            return $this->db->resultSet();
        }

        public function racersdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 13");
            return $this->db->resultSet();
        }

        //=========================wings======================================================

        public function wingscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 15");
            return $this->db->resultSet();
        }

        public function wingsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 15");
            return $this->db->resultSet();
        }

        public function wingsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 15");
            return $this->db->resultSet();
        }

        public function wingsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 15");
            return $this->db->resultSet();
        }

        public function wingsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 15");
            return $this->db->resultSet();
        }

        public function designcontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 7");
            return $this->db->resultSet();
        }

        public function designuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 7");
            return $this->db->resultSet();
        }

        public function designusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 7");
            return $this->db->resultSet();
        }

        public function designImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 7");
            return $this->db->resultSet();
        }

        public function designdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 7");
            return $this->db->resultSet();
        }
        
         //=========================mods======================================================

         public function modscontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 11");
            return $this->db->resultSet();
        }

        public function modsuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 11");
            return $this->db->resultSet();
        }

        public function modsusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 11");
            return $this->db->resultSet();
        }

        public function modsImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 11");
            return $this->db->resultSet();
        }

        public function modsdownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 11");
            return $this->db->resultSet();
        }

         //=========================code======================================================

         public function codecontent() {
            $this->db->query("SELECT * FROM tbl_Content where POSTID = 6");
            return $this->db->resultSet();
        }

        public function codeuname() {
            $this->db->query("SELECT u.UNAME, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 6");
            return $this->db->resultSet();
        }

        public function codeusercont() {
            $this->db->query("SELECT u.UNAME, u.POSTNO, u.COMENTSNO, p.userID, p.POSTID
            FROM tbl_user u
            LEFT OUTER JOIN tbl_posts p on u.userID=p.userID
            where POSTID = 6");
            return $this->db->resultSet();
        }

        public function codeImgCara() {
            $this->db->query("SELECT * FROM tbl_imgtable where POSTID = 6");
            return $this->db->resultSet();
        }

        public function codedownload() {
            $this->db->query("SELECT * FROM tbl_downloads where POSTID = 6");
            return $this->db->resultSet();
        }


    }

?>