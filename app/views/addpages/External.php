<?php require APPROOT . "/views/inc/header.php"; ?>

<?php foreach ($data['external'] as $item) { ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php echo $item['adptitle']; ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <img src="<?php echo URLROOT; echo $item['adpimg'];?>" alt="US">
            </div>
            <div class="col-8">
                <p> <?php echo $item['adpcont']; ?></p>
            </div>
        </div>
    </div>    
<?php
} ?>


<?php require APPROOT . "/views/inc/footer.php"; ?>