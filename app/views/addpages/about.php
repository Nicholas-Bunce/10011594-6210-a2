<?php require APPROOT . "/views/inc/header.php"; ?>

<?php foreach ($data['about'] as $item) { ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1><?php echo $item['adptitle']; ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <img src="<?php echo URLROOT; echo $item['adpimg'];?>" alt="US">
            </div>
            <div class="col-8">
                <p> <?php echo $item['adpcont']; ?></p>
            </div>
        </div>
    </div>    
<?php
} ?>

<div class="row">
    <div class="col">
        <script>
            function initMap() {
                var wellington= {lat: -41.2865, lng: 174.7762};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: wellington
                });
                var marker = new google.maps.Marker({
                    position: wellington,
                    map: map
                });
                }
                </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVdNJid9oL7KIwwViL2vm_fm-A--VNN24&callback=initMap">
        </script>
    </div>
</div>    


<?php require APPROOT . "/views/inc/footer.php"; ?>
