<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo URLROOT;?>/css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
  <title>MakerMan</title>
</head>

<body id="textover">
  <nav class="navbar navbar-expand navbar-fixed-top navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <div class="navbar-nav">
        <div class="navbar-nav mr auto dropdown nav-logo ">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="logo">
              <img src="<?php echo URLROOT;?>/images/2000px-Adidas_Logo.svg.png" height="20" alt="">
            </div>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" id="bootover">
            
            <?php
                foreach($data['navinfo1'] as $item) {?>

                    <a class='dropdown-item' href="<?php echo URLROOT; echo $item['navLink']?>"> <?php echo $item['navName']?></a>
                   
                  <?php }
            ?>
          </div>
        </div>
      </div>
      <div class="mx-auto">
        <a class="name" href="Home.html">MakerForge</a>
      </div>
      <div class="navbar-nav ">
        <a class="btn btn-success" href="<?php echo URLROOT; ?> pages/display" role="button">Login</a>
      </div>
    </div>
  </nav>
  <div class="subthreadlink container-flex">
    <div class="row" id="textcolourover">
        <?php
              foreach($data['navSub'] as $item) {?>

                   <a class='col' href= "<?php echo URLROOT; echo $item['SubLink']?>"> <?php echo $item['SubcatDescription']?></a>
                
                <?php }
              ?>
    </div>
  </div>