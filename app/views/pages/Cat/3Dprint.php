<?php require APPROOT . "/views/inc/header.php"; ?>
<div class="container">
        <?php foreach ($data['Sub'] as $item){ ?>

        <div class="row">
            <div class="post-tab col border border-dark rounded">
                <div class="row" >
                    <div class="col-2 ptab-img" id="subcat">
                    <img src= "<?php echo URLROOT; echo $item['Subimg'];?>" alt="" class="rounded" >
                    </div>
                    <div class="pdecription col-10">
                        <a class='col h1' href="<?php echo URLROOT; echo $item['SubLink']?>" > <?php echo $item['SubcatDescription']?></a>
                    </div>
                </div>
            </div>
        </div>  
    <?php } ?>
  </div>

<?php require APPROOT . "/views/inc/footer.php"; ?>