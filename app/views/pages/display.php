<?php require APPROOT . "/views/inc/header.php"; ?>

    <div class="container">
      <div class="row">
        <div class="col">
          <p>This website is design as a free space for creativity. This forum is design to grow and evolve as the user base increases, as such it is based on a php MVC framework so to allow for growth. But there is another side of this website, so to allow those people let their creativity loose even with the restrictions of the modern world as the Maker forge family has a brick and mortar location that have builder competitions and more, enjoy.</p>
        </div>
      </div>
  <?php foreach ($data['Main'] as $item){ ?>
    <div class="row">
          <div class="post-tab col border border-dark rounded">
            <div class="row">
              <div class="col-2 ptab-img">
                <img src= "<?php echo URLROOT; echo $item['IMG'];?>" alt="" class="rounded">
              </div>
              <div class="pdecription col-8">
                <h4><?php echo $item['PNAME']?></h4>
                <p> <?php echo $item['PDescription'] ?> </p>
              </div>
              <div class="pcounter border border-danger rounded col" id="pcounter">
                <a class="Username" href="Home.html"><?php echo $item['PNAME']?></a>
              </div>
            </div>
          </div>
        </div>
  <?php
  } ?>

<?php require APPROOT . "/views/inc/footer.php"; ?>