<?php require APPROOT . "/views/inc/header.php"; ?>

      <div class="cont-head">
      <?php foreach ($data['name'] as $item) { ?>
          <H4><?php echo $item['UNAME'] ?></H4>
      <?php }
      ?>
        
    </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9" role="main">
                <div class="carousel slide" id="carousel">
                    <div class="carousel-inner">
                        <?php foreach ($data['Imgcara'] as $item) { ?>
                            <div class="carousel-item active">
                                <img src="<?php echo URLROOT; echo $item['IMG']; ?>" alt="slide">
                            </div>  
                      <?php  } ?>

                    </div>

                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                </div>

                <?php foreach ($data['content'] as $item) { ?>
                    <div class="text">
                        <p><?php echo $item['Content'];?></p>
                    </div> 
                     <?php } ?>
                </div>
             
            <div class=""></div>
            <div class="col-md-3">
                <div class="profile border border-dark rounded">
                    <img src="images/p (2).JPG" alt="" class="userimg">
                    <div class="userinfo" id="info">
                    <?php foreach ($data['name'] as $item) { ?>
                        <p class="h4"> <?php echo $item['UNAME']; ?></p>
                        <?php }
                        ?>
                        <ul>
                            <?php foreach ($data['userinfo'] as $item) { ?> 
                            <li><?php echo $item['POSTNO']; ?></li>
                            <li><?php echo $item['COMENTSNO']; ?></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="downloads border border-dark rounded" id="info">
                    <p class="h4">downloads</p>
                    <div class="downlinks border border-dark rounded">
                        <ul>
                            <?php foreach ($data['downloads'] as $item) { ?>
                                <li><a href="<?php echo $item['dLink']; ?>"><?php echo $item['dNAME']; ?></a></li>
                            <?php }
                            ?>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require APPROOT . "/views/inc/footer.php"; ?>