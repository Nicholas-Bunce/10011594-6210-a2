# Folder Structure 

Since git stores files and not folders, run the following command to create the folders in your terminal (git bash)

For the app folder

```
mkdir -p app/configs && \
mkdir -p app/controllers && \
mkdir -p app/models && \
mkdir -p app/views && \
mkdir -p app/libraries && \
mkdir -p app/helpers
```

For the public folder

```
mkdir -p public/css && \
mkdir -p public/errors && \
mkdir -p public/images && \
mkdir -p public/scripts
```

Create the following files in the folders (copy and paste the code below)

```
touch app/libraries/Controller.php && \
touch app/libraries/Database.php && \
touch app/libraries/Core.php && \
touch bootstrap.php
touch public/index.php
```